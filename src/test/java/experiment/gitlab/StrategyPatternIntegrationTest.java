package experiment.gitlab;

import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;


import org.junit.ClassRule;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class StrategyPatternIntegrationTest {

    @ClassRule
    public static final DropwizardAppRule<strategyPatternConfiguration> RULE =
            new DropwizardAppRule<>(strategyPatternApplication.class,
                    ResourceHelpers.resourceFilePath("config.yml"));

    @Test
    public void strategyIndex() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("test client");

        Response response = client.target(
                String.format("http://localhost:%d/strategy", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(200);
    }
}
