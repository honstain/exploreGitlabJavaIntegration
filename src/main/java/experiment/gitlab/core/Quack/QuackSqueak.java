package experiment.gitlab.core.Quack;

public class QuackSqueak implements QuackBehavior {

    public String quack() {
        return "Squeak";
    }
}
