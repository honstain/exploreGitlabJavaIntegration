package experiment.gitlab.core;

import experiment.gitlab.api.DuckSimulator;
import experiment.gitlab.core.Flight.FlyBehavior;
import experiment.gitlab.core.Quack.QuackBehavior;

public class Duck {

    private String description;
    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public Duck(String description, FlyBehavior flyBehavior, QuackBehavior quackBehavior) {
        this.description = description;
        this.flyBehavior = flyBehavior;
        this.quackBehavior = quackBehavior;
    }

    public DuckSimulator simulate() {
        return new DuckSimulator(
                getDescription(),
                performFly(),
                performQuack()
        );
    }

    private String getDescription() {
        return description;
    }

    private String performFly() {
        return flyBehavior.fly();
    }

    private String performQuack() {
        return quackBehavior.quack();
    }
}
