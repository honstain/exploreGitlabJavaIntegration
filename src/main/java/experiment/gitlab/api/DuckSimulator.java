package experiment.gitlab.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DuckSimulator {

    private String duckDescription;
    private String noise;
    private String flight;

    public DuckSimulator() {
        // Jackson deserialization
    }

    public DuckSimulator(String duckDescription, String noise, String flight) {
        this.duckDescription = duckDescription;
        this.noise = noise;
        this.flight = flight;
    }

    @JsonProperty
    public String getDuckDescription() {
        return duckDescription;
    }

    @JsonProperty
    public String getNoise() {
        return noise;
    }

    @JsonProperty
    public String getFlight() {
        return flight;
    }
}
