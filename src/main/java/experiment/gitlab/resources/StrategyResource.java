package experiment.gitlab.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.ImmutableMap;
import experiment.gitlab.api.DuckSimulator;
import experiment.gitlab.core.*;
import experiment.gitlab.core.Flight.FlyNotPossible;
import experiment.gitlab.core.Flight.FlyWithWings;
import experiment.gitlab.core.Quack.Quack;
import experiment.gitlab.core.Quack.QuackMute;
import experiment.gitlab.core.Quack.QuackSqueak;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;

@Path("/strategy")
@Produces(MediaType.APPLICATION_JSON)
public class StrategyResource {

    private ImmutableMap<String, Duck> supportedDucks = ImmutableMap.of(
            "stubDuck", new Duck("stubDuck", new FlyNotPossible(), new QuackMute()),
            "mallard", new Duck("mallard", new FlyWithWings(), new Quack()),
            "rubberDuck", new Duck("rubberDuck", new FlyNotPossible(), new QuackSqueak()),
            "decoyDuck", new Duck("decoyDuck", new FlyNotPossible(), new QuackMute())
    );

    @GET
    @Timed
    public Set<String> index() {
        return supportedDucks.keySet();
    }

    @Path("/{description}")
    @GET
    @Timed
    public DuckSimulator simulate(@PathParam("description") String description) {
        Duck duck = supportedDucks.get(description);
        if (duck == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return duck.simulate();
    }
}
