package experiment.gitlab;

import experiment.gitlab.resources.StrategyResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class strategyPatternApplication extends Application<strategyPatternConfiguration> {

    public static void main(final String[] args) throws Exception {
        new strategyPatternApplication().run(args);
    }

    @Override
    public String getName() {
        return "strategyPattern";
    }

    @Override
    public void initialize(final Bootstrap<strategyPatternConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final strategyPatternConfiguration configuration,
                    final Environment environment) {

        environment.jersey().register(new StrategyResource());
    }

}
